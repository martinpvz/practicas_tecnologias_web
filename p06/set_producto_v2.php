<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html;charset=utf-8" />
		<title>Registro</title>
	</head>
	<body>
  <?php
  $nombre = $_POST['nombre']; $marca = $_POST['marca']; $modelo = $_POST['modelo']; $precio = $_POST['precio']; $detalles = $_POST['detalles']; $unidades = $_POST['unidades']; $imagen = $_POST['imagen'];
  $error = false;
  if($nombre == '') {
    echo '<h3>El nombre no fue ingresado</h3>';
    $error = true;
  } 
  if($marca == '') {
    echo '<h3>La marca no fue ingresada</h3>';
    $error = true;
  }
  if($modelo == '') {
    echo '<h3>El modelo no fue ingresado</h3>';
    $error = true;
  }
  if($precio == '') {
    echo '<h3>El precio no fue ingresado</h3>';
    $error = true;
  }
  if(strpos($precio, '.') === false) {
    echo '<h3>El precio no tiene punto decimal</h3>';
    $error = true;
  }
  if($detalles == '') {
    echo '<h3>Los detalles no fueron ingresados</h3>';
    $error = true;
  }
  if(!ctype_digit($unidades)) {
    echo '<h3>Las unidades no fueron ingresadas</h3>';
    $error = true;
  }
  if($imagen == '') {
    echo '<h3>La imagen no fue ingresada</h3></br>';
    $error = true;
  }
  if(!$error) {
  @$link = new mysqli('localhost', 'root', 'Martin.13', 'marketzone');
  if ($link->connect_errno) 
  {
      die('Falló la conexión: '.$link->connect_error.'<br/>');
  }

  $sql = "INSERT INTO productos VALUES (null, '{$nombre}', '{$marca}', '{$modelo}', {$precio}, '{$detalles}', {$unidades}, '{$imagen}', '0')";
  if ( $link->query($sql) )
  {
      echo '<h2>Los datos ingresados son:</h2>';
      echo '<ul>';
      echo '<li>ID: '.$link->insert_id. '</li>';
      echo '<li>Nombre: '.$nombre. '</li>';
      echo '<li>Marca: '.$marca. '</li>';
      echo '<li>Modelo: '.$modelo. '</li>';
      echo '<li>Precio: '.$precio. '</li>';
      echo '<li>Detalles: '.$detalles. '</li>';
      echo '<li>Unidades: '.$unidades. '</li>';
      echo '<li>Imagen: '.$imagen. '</li>';
      echo '</ul>';
  }
  else
  {
    echo 'El Producto no pudo ser insertado =(';
  }

  $link->close();
  }

  ?>
	</body>
</html>