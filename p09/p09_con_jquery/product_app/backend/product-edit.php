<?php
    include_once __DIR__.'/database.php';

    // SE OBTIENE LA INFORMACIÓN DEL PRODUCTO ENVIADA POR EL CLIENTE
    $producto = file_get_contents('php://input');
    $name = $_POST['name'];
    $description = $_POST['description'];
    $jsonOBJ = json_decode($description);
    $id = $_POST['id'];
    $data = array(
      'status'  => 'error',
      'message' => 'La consulta falló'
    );


    $sql = "UPDATE productos SET nombre = '{$name}', marca = '{$jsonOBJ->marca}', modelo = '{$jsonOBJ->modelo}', precio = {$jsonOBJ->precio}, detalles = '{$jsonOBJ->detalles}', unidades = {$jsonOBJ->unidades}, imagen = '{$jsonOBJ->imagen}', eliminado = 0 WHERE id = '{$id}'";

    if($conexion->query($sql)){
      $data['status'] =  "success";
      $data['message'] =  "Producto modificado";
    } else {
      $data['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($conexion);
    }
    
    $conexion->close();
    echo json_encode($data, JSON_PRETTY_PRINT);

?>