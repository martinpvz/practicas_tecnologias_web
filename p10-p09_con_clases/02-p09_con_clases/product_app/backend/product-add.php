<?php
    use API\Create\Crear as Crear;
    require_once __DIR__.'/API/start.php';

    $productos = new Crear('marketzone');
    $productos->add( json_decode( json_encode($_POST) ) );
    echo $productos->getResponse();
?>