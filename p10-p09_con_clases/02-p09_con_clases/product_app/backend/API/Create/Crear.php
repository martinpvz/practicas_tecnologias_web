<?php
namespace API\Create;
use API\DataBase;
require_once __DIR__.'/../DataBase.php';
class Crear extends Database {
    public function add( $jsonOBJ ) {
        $data = array(
          'status'  => 'error',
          'message' => 'Ya existe un producto con ese nombre'
        );
        // SE ASUME QUE LOS DATOS YA FUERON VALIDADOS ANTES DE ENVIARSE
        $sql = "SELECT * FROM productos WHERE nombre = '{$jsonOBJ->nombre}' AND eliminado = 0";
        $result = $this->conexion->query($sql);
        
        if ($result->num_rows == 0) {
            $this->conexion->set_charset("utf8");
            $sql = "INSERT INTO productos VALUES (null, '{$jsonOBJ->nombre}', '{$jsonOBJ->marca}', '{$jsonOBJ->modelo}', {$jsonOBJ->precio}, '{$jsonOBJ->detalles}', {$jsonOBJ->unidades}, '{$jsonOBJ->imagen}', 0)";
            if($this->conexion->query($sql)){
                $data['status'] =  "success";
                $data['message'] =  "Producto agregado";
            } else {
                $data['message'] = "ERROR: No se ejecuto $sql. " . mysqli_error($this->conexion);
            }
    
            $result->free();
            // Cierra la conexion
            $this->conexion->close();
        }
        $this->response = $data;
        // SE HACE LA CONVERSIÓN DE ARRAY A JSON
        // echo json_encode($data, JSON_PRETTY_PRINT);
      }
}

?>