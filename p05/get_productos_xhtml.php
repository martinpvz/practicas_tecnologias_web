<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<?php
  if(isset($_GET["tope"]))
    $tope = $_GET["tope"];
  
  if(!empty($tope)) {
    @$link = new mysqli("localhost", "root", "Martin.13", "marketzone");

    if($link->connect_errno) {
      die("La conexion no pudo establecerse");
    }

    if($result = $link->query("SELECT * FROM productos WHERE unidades <= {$tope}")) {
      $row = $result->fetch_all(MYSQLI_ASSOC);
      $result->free();
    }

    $link->close();
  }
	?>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Producto</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	</head>
	<body>
		<h3>PRODUCTOS</h3>

		<br/>
		
		<?php if(isset($row)):
    foreach ($row as $key => $value) {?>

			<table class="table">
				<thead class="thead-dark">
					<tr>
					<th scope="col">#</th>
					<th scope="col">Nombre</th>
					<th scope="col">Marca</th>
					<th scope="col">Modelo</th>
					<th scope="col">Precio</th>
					<th scope="col">Unidades</th>
					<th scope="col">Detalles</th>
					<th scope="col">Imagen</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th scope="row"><?= $row[$key]['id'] ?></th>
						<td><?= $row[$key]['nombre'] ?></td>
						<td><?= $row[$key]['marca'] ?></td>
						<td><?= $row[$key]['modelo'] ?></td>
						<td><?= $row[$key]['precio'] ?></td>
						<td><?= $row[$key]['unidades'] ?></td>
						<td><?= $row[$key]['detalles'] ?></td>
						<td><img src="<?= $row[$key]['imagen']?>" alt="Imagen de producto"></td>
					</tr>
				</tbody>
			</table>
		<?php }elseif(!empty($id)) : ?>

			<script>
                alert('El ID del producto no existe');
            </script>

		<?php endif; ?>
	</body>
</html>