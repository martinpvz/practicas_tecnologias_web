<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="./style.css">
  <title>Registrar Producto</title>
</head>
<body>
  <h1>Bienvenido al formulario de registro de productos</h1>
  <main>
    <form action="./update_producto.php" method="post">
      <label for="id_form">Id: </label>
      <input type="text" name="id" id="id_form" class="id_f" readonly value="<?= !empty($_POST['id'])?$_POST['id']:$_GET['id'] ?>">
      <label for="nombre_form">Nombre: </label>
      <input type="text" name="nombre" id="nombre_form" onblur="comprobarNombre()" value="<?= !empty($_POST['nombre'])?$_POST['nombre']:$_GET['nombre'] ?>">
      <p id="avisoNombre" class="aviso"></p>
      <!-- <label for="marca_form">Marca: </label>
      <input type="text" name="marca" id="marca_form"> -->
      <label for="marca_form">Marca: </label>
      <input list="marca" name="marca" id="marca_form" onblur="comprobarMarca()" value="<?= !empty($_POST['marca'])?$_POST['marca']:$_GET['marca'] ?>">
      <datalist id="marca">
        <option value="Apple"></option>
        <option value="Samsung"></option>
        <option value="Xiaomi"></option>
        <option value="OnePlus"></option>
        <option value="Oppo"></option>
      </datalist>
      <p id="avisoMarca" class="aviso"></p>
      <label for="modelo_form">Modelo: </label>
      <input type="text" name="modelo" id="modelo_form" onblur="comprobarModelo()" value="<?= !empty($_POST['modelo'])?$_POST['modelo']:$_GET['modelo'] ?>">
      <p id="avisoModelo" class="aviso"></p>
      <label for="precio_form">Precio: </label>
      <input type="number" name="precio" id="precio_form" min="0" step="0.01" onblur="comprobarPrecio()" value="<?= !empty($_POST['precio'])?$_POST['precio']:$_GET['precio'] ?>">
      <p id="avisoPrecio" class="aviso"></p>
      <label for="detalles_form">Detalles: </label>
      <input type="text" name="detalles" id="detalles_form" onblur="comprobarDetalles()" value="<?= !empty($_POST['detalles'])?$_POST['detalles']:$_GET['detalles'] ?>">
      <p id="avisoDetalles" class="aviso"></p>
      <label for="unidades_form">Unidades: </label>
      <input type="number" name="unidades" id="unidades_form" min="0" onblur="comprobarUnidades()" value="<?= !empty($_POST['unidades'])?$_POST['unidades']:$_GET['unidades'] ?>">
      <p id="avisoUnidades" class="aviso"></p>
      <label for="imagen_form">Imagen: </label>
      <input type="text" name="imagen" id="imagen_form" onblur="comprobarImagen()" value="<?= !empty($_POST['imagen'])?$_POST['imagen']:$_GET['imagen'] ?>">
      <p id="avisoImagen" class="aviso"></p>
      <input type="submit" value="Registrar" class='boton-enviar'>
    </form>
  </main>
  <script src="./main.js"></script>
</body>
</html>