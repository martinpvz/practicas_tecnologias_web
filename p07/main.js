const nombre = document.getElementById('nombre_form');
const marca = document.getElementById('marca_form');
const modelo = document.getElementById('modelo_form');
const precio = document.getElementById('precio_form');
const detalles = document.getElementById('detalles_form');
const unidades = document.getElementById('unidades_form');
const imagen = document.getElementById('imagen_form');

function comprobarNombre() {
  if (nombre.value === '') {
    document.getElementById("avisoNombre").innerHTML = 'El nombre es obligatorio';
  }
  else if(nombre.value.length >= 100) {
    document.getElementById("avisoNombre").innerHTML = 'Solo se permiten maximo 100 caracteres';
  }
  else {
    document.getElementById("avisoNombre").innerHTML = '';
  }
}

function comprobarMarca() {
  if (marca.value === '') {
    document.getElementById("avisoMarca").innerHTML = 'El nombre es obligatorio';
  }
  else {
    document.getElementById("avisoMarca").innerHTML = '';
  }
}

function comprobarModelo() {
  if (modelo.value === '') {
    document.getElementById("avisoModelo").innerHTML = 'La marca es obligatoria';
  }
  else if(modelo.value.length >= 25) {
    document.getElementById("avisoModelo").innerHTML = 'Solo se permiten maximo 25 caracteres';
  }
  else {
    document.getElementById("avisoModelo").innerHTML = '';
  }
}

function comprobarPrecio() {
  if (precio.value === '') {
    document.getElementById("avisoPrecio").innerHTML = 'El precio es obligatorio';
  }
  else if(parseFloat(precio.value) <= 99.99) {
    document.getElementById("avisoPrecio").innerHTML = 'El precio debe ser mayor a 99.99';
  }
  else {
    document.getElementById("avisoPrecio").innerHTML = '';
  }
}

function comprobarDetalles() {
  if (detalles.value.length >= 250) {
    document.getElementById("avisoDetalles").innerHTML = 'Solo se permiten maximo 250 caracteres';
  }
  else {
    document.getElementById("avisoDetalles").innerHTML = '';
  }
}

function comprobarUnidades() {
  if (unidades.value === '') {
    document.getElementById("avisoUnidades").innerHTML = 'Las unidades son obligatorias';
  }
  else if(parseFloat(unidades.value) < 0) {
    document.getElementById("avisoUnidades").innerHTML = 'Las unidades deben ser mayor o igual a 0';
  }
  else {
    document.getElementById("avisoUnidades").innerHTML = '';
  }
}

function comprobarImagen() {
  if (imagen.value === '') {
    imagen.value = './img/imagenOpcional.jpg';
  }
}

function show() {
  // se obtiene el id de la fila donde está el botón presinado
  var rowId = event.target.parentNode.parentNode.id;

  // se obtienen los datos de la fila en forma de arreglo
  var data = document.getElementById(rowId).querySelectorAll(".row-data");
  /**
  querySelectorAll() devuelve una lista de elementos (NodeList) que 
  coinciden con el grupo de selectores CSS indicados.
  (ver: https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors)

  En este caso se obtienen todos los datos de la fila con el id encontrado
  y que pertenecen a la clase "row-data".
  */

  // var name = data[0].innerHTML;
  // var age = data[1].innerHTML;

  // alert("Name: " + name + "\nAge: " + age);

  // send2form(name, age);
  var id = data[0].innerHTML;
  var nombre = data[1].innerHTML;
  var marca = data[2].innerHTML;
  var modelo = data[3].innerHTML;
  var precio = data[4].innerHTML;
  var unidades = data[5].innerHTML;
  var detalles = data[6].innerHTML;
  var imagen = data[7].getAttribute('src');
  console.log(imagen);
  // imagen = imagen.slice(9, 40);
  // var img = toString(imagen);

  alert("id: " + id + "\nNombre: " + nombre + "\nMarca: " + marca + "\nModelo: " + modelo + "\nPrecio: " + precio + "\nUnidades: " + unidades + "\nDetalles: " + detalles + "\nImagen: " + imagen);

  send2form(id, nombre, marca, modelo, precio, unidades, detalles, imagen);
}

function send2form(id,nombre, marca, modelo, precio, unidades, detalles, imagen) {
  var form = document.createElement("form");

  var idIn = document.createElement("input");
  idIn.type = 'text';
  idIn.name = 'id';
  idIn.value = id;
  form.appendChild(idIn);

  var nombreIn = document.createElement("input");
  nombreIn.type = 'text';
  nombreIn.name = 'nombre';
  nombreIn.value = nombre;
  form.appendChild(nombreIn);

  var marcaIn = document.createElement("input");
  marcaIn.type = 'text';
  marcaIn.name = 'marca';
  marcaIn.value = marca;
  form.appendChild(marcaIn);

  var modeloIn = document.createElement("input");
  modeloIn.type = 'text';
  modeloIn.name = 'modelo';
  modeloIn.value = modelo;
  form.appendChild(modeloIn);
  
  var precioIn = document.createElement("input");
  precioIn.type = 'number';
  precioIn.name = 'precio';
  precioIn.value = precio;
  form.appendChild(precioIn);

  var unidadesIn = document.createElement("input");
  unidadesIn.type = 'text';
  unidadesIn.name = 'unidades';
  unidadesIn.value = unidades;
  form.appendChild(unidadesIn);

  var detallesIn = document.createElement("input");
  detallesIn.type = 'text';
  detallesIn.name = 'detalles';
  detallesIn.value = detalles;
  form.appendChild(detallesIn);

  var imagenIn = document.createElement("input");
  imagenIn.type = 'text';
  imagenIn.name = 'imagen';
  imagenIn.value = imagen;
  form.appendChild(imagenIn);

  console.log(form);

  form.method = 'POST';
  form.action = './formulario_productos_v3.php';  

  document.body.appendChild(form);
  form.submit();
}