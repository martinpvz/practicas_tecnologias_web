<?php
function multiplo($numero) {
  if(isset($numero)) {
    if($numero % 5 == 0 and $numero % 7 == 0) {
      echo '<p>El número '. $numero .' es múltiplo de 5 y 7</p>';
    } else {
      echo '<p>El número '. $numero .' no es múltiplo de 5 y 7</p>';
    }
  } else {
    echo '<p>No se ha ingresado ningún valor</p>';
  }
}

function generarMatriz() {
  $matriz = [];
  // $finaliza = true;
  do {
    $a = rand(1, 1000);$b = rand(1, 1000);$c = rand(1, 1000);
    $matrizProv = [$a, $b, $c];
    $matriz[] = $matrizProv;
    if($a % 2 != 0 and $b % 2 == 0 and $c % 2 != 0) {
      break;
    }
  } while(true);
  echo '<p>impar, par, impar</p>';
  $elementos = 0;
  foreach ($matriz as $value) {
    $elementos = $elementos + count($value);
    foreach($value as $numero) {
      echo '<span>&nbsp;'. $numero.   '&nbsp;&nbsp;&nbsp;&nbsp;</span>';
    }
    echo '<br>';
  }
  echo '<p>'. $elementos .' números obtenidos en '. count($matriz). ' iteraciones</p>';
}

function aleatorios($multiplo) {
  if(isset($multiplo)) {
    while (true) {
      $numero = rand(1,1000);
      if($numero % $multiplo == 0) {
        echo '<p>El número aleatorio '. $numero. ' es múltiplo de '. $multiplo .' (hecho con while).</p>';
        break;
      }
    }
  } else {
    echo '<p>No se ha ingresado ningún valor</p>';
  }
}

function aleatoriosDoWhile($multiplo) {
  if(isset($multiplo)) {
    do {
      $numero = rand(1,1000);
      if($numero % $multiplo == 0) {
        echo '<p>El número aleatorio '. $numero. ' es múltiplo de '. $multiplo .' (hecho con do-while).</p>';
        break;
      }
    } while(true);
  } else {
    echo '<p>No se ha ingresado ningún valor</p>';
  }
}

function arregloTabla() {
  $arreglo = [];
  for ($i=97; $i<123; $i++) { 
    $arreglo[$i] = chr($i);
  }
  echo '<table style="border:1px solid black;>';
  foreach ($arreglo as $key => $value) {
    echo '<tr style="border:1px solid black;">';
    echo '<td style="border:1px solid black; width:50px;">'. $key .'</td><td style="border:1px solid black;width:50px;">'. $value .'</td>';
    echo '</tr>';
  }
  echo "</table>";
}
?>
