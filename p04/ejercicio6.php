<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.1//EN”
“http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd”>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang=“es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title> Ejercicio 6 </title>
</head>
<body>
<h1> Ejercicio 6 </h1>
<?php
error_reporting(E_ERROR | E_PARSE);
$matricula = $_POST["matricula"];
$opcion = $_POST["opcion"];
$parqueVehicular = [
  'ABC1001' => ['Auto' => ['marca' => 'HONDA', 'modelo' => 2020, 'tipo' => 'Camioneta'], 'Propietario' => ['nombre' => 'Martin Paniagua', 'ciudad' => 'Tuxtla Gutierrez', 'direccion' => 'Las Flores 166']],
  'ABC1002' => ['Auto' => ['marca' => 'HONDA', 'modelo' => 2012, 'tipo' => 'Sedan'], 'Propietario' => ['nombre' => 'Citlalli Lopez', 'ciudad' => 'Panotla', 'direccion' => 'Las Flores 200']],
  'ABC1003' => ['Auto' => ['marca' => 'CHEVROLET', 'modelo' => 2008, 'tipo' => 'Camioneta'], 'Propietario' => ['nombre' => 'John Morales', 'ciudad' => 'Puebla', 'direccion' => 'Las Flores 210']],
  'ABC1004' => ['Auto' => ['marca' => 'KIA', 'modelo' => 2019, 'tipo' => 'Sedan'], 'Propietario' => ['nombre' => 'Alheli Moreno', 'ciudad' => 'Puebla', 'direccion' => 'Las Flores 212']],
  'ABC1005' => ['Auto' => ['marca' => 'CHEVROLET', 'modelo' => 2020, 'tipo' => 'Hachback'], 'Propietario' => ['nombre' => 'Manuel Salinas', 'ciudad' => 'Cuernavaca', 'direccion' => 'Las Flores 300']],
  'ABC1006' => ['Auto' => ['marca' => 'FORD', 'modelo' => 2012, 'tipo' => 'Camioneta'], 'Propietario' => ['nombre' => 'Alfredo Ceron', 'ciudad' => 'Los Cabos', 'direccion' => 'Las Flores 400']],
  'ABC1007' => ['Auto' => ['marca' => 'KIA', 'modelo' => 2010, 'tipo' => 'Hachback'], 'Propietario' => ['nombre' => 'Maria Cisneros', 'ciudad' => 'Merida', 'direccion' => 'Las Flores 500']],
  'ABC1008' => ['Auto' => ['marca' => 'AUDI', 'modelo' => 2017, 'tipo' => 'Hachback'], 'Propietario' => ['nombre' => 'Iliana Velazquez', 'ciudad' => 'Guaymas', 'direccion' => 'Las Flores 600']],
  'ABC1009' => ['Auto' => ['marca' => 'FORD', 'modelo' => 2002, 'tipo' => 'Camioneta'], 'Propietario' => ['nombre' => 'Jose Perez', 'ciudad' => 'Campeche', 'direccion' => 'Las Flores 700']],
  'ABC1010' => ['Auto' => ['marca' => 'CHEVROLET', 'modelo' => 2021, 'tipo' => 'Sedan'], 'Propietario' => ['nombre' => 'Roberto Antuna', 'ciudad' => 'Monterrey', 'direccion' => 'Las Flores 800']],
  'ABC1011' => ['Auto' => ['marca' => 'HONDA', 'modelo' => 2004, 'tipo' => 'Sedan'], 'Propietario' => ['nombre' => 'Roberto Alvarado', 'ciudad' => 'Guanajuato', 'direccion' => 'Las Flores 900']],
  'ABC1012' => ['Auto' => ['marca' => 'FORD', 'modelo' => 2000, 'tipo' => 'Camioneta'], 'Propietario' => ['nombre' => 'Javier Hernandez', 'ciudad' => 'Coatzacoalcos', 'direccion' => 'Las Flores 291']],
  'ABC1013' => ['Auto' => ['marca' => 'HONDA', 'modelo' => 2016, 'tipo' => 'Sedan'], 'Propietario' => ['nombre' => 'Tamily Gonzalez', 'ciudad' => 'Comitan', 'direccion' => 'Las Flores 910']],
  'ABC1014' => ['Auto' => ['marca' => 'KIA', 'modelo' => 2020, 'tipo' => 'Camioneta'], 'Propietario' => ['nombre' => 'Jesus Corona', 'ciudad' => 'Tuxtla Gutierrez', 'direccion' => 'Las Flores 912']],
  'ABC1015' => ['Auto' => ['marca' => 'AUDI', 'modelo' => 2022, 'tipo' => 'Sedan'], 'Propietario' => ['nombre' => 'El bicho', 'ciudad' => 'Villahermosa', 'direccion' => 'Las Flores 101']],
];
function buscarMatricula($matricula, $arreglo) {
  $noencontrado = true;
  foreach ($arreglo as $key => $value) {
    if($matricula == $key) {
      echo "<h2>El automóvil es: </h2>";
      echo "<h4>Matrícula: ". $key. "</h4>";
      echo "<h4>Marca: ". $value['Auto']['marca']. "</h4>";
      echo "<h4>Modelo: ". $value['Auto']['modelo']. "</h4>";
      echo "<h4>Tipo: ". $value['Auto']['tipo']. "</h4>";
      echo "<h2>El propietario es: </h2>";
      echo "<h4>Nombre: ". $value['Propietario']['nombre']. "</h4>";
      echo "<h4>Ciudad: ". $value['Propietario']['ciudad']. "</h4>";
      echo "<h4>Dirección: ". $value['Propietario']['direccion']. "</h4>";
      $noencontrado = false;
      break;
    }
  }
  if($noencontrado) {
    echo "<h3>No se encontró ningún automóvil con esa matrícula.</h3>";
  }
}

function mostrarTodos($arreglo) {
  foreach ($arreglo as $key => $value) {
    echo "<h3>Matrícula: ". $key. "</h3>";
    echo "<h4>Marca: ". $value['Auto']['marca']. "</h4>";
    echo "<h4>Modelo: ". $value['Auto']['modelo']. "</h4>";
    echo "<h4>Tipo: ". $value['Auto']['tipo']. "</h4>";
    echo "<h2>El propietario es: </h2>";
    echo "<h4>Nombre: ". $value['Propietario']['nombre']. "</h4>";
    echo "<h4>Ciudad: ". $value['Propietario']['ciudad']. "</h4>";
    echo "<h4>Dirección: ". $value['Propietario']['direccion']. "</h4>";
    echo "<hr>";
  }
}

if($opcion == "matricula" and $matricula != "") {
  buscarMatricula($matricula, $parqueVehicular);
}
elseif($matricula == "") {
  echo "<h2>La matrícula no se ingresó.</h2>";
}
elseif($opcion == "todos") {
  mostrarTodos($parqueVehicular);
}
?>
</body>
</html>