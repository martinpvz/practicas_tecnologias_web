<!DOCTYPE html PUBLIC “-//W3C//DTD XHTML 1.1//EN”
“http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd”>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang=“es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title> Ejercicios Práctica 03 </title>
</head>
<body>
<h2> Ejercicio 1 </h2>
<?php
error_reporting(E_ERROR | E_PARSE);
include './p04_funciones.php';
$numero = $_GET["numero"];
multiplo($numero);
?>
<h2>Ejercicio 2</h2>
<?php
generarMatriz();
?>
<h2>Ejercicio 3</h2>
<?php
aleatorios($numero);
aleatoriosDoWhile($numero);
?>
<h2>Ejercicio 4</h2>
<?php
arregloTabla();
?>
<h2>Ejercicio 5</h2>
<form action="./ejercicio5.php" method="post">
  <label for="sexo">Ingrese su sexo:</label>
  <input type="radio" name="sexo" id="masculino" value="Masculino">
  <label for="masculino">Masculino</label>
  <input type="radio" name="sexo" id="femenino" value="Femenino">
  <label for="femenino">Femenino</label>
  <br><br>
  <label for="edad">Edad:</label>
  <input type="number" id="edad" name="edad" min="1" max="100">
  <br><br>
  <input type="submit" value="Enviar">
</form>

<h2>Ejercicio 6</h2>
<form action="./ejercicio6.php" method="post">
  <label for="opcmatricula">Seleccionar opción: </label>
  <input type="radio" name="opcion" id="opcmatricula" value="matricula">
  <label for="opcmatricula">Buscar por matrícula.</label>
  <input type="radio" name="opcion" id="opctodos" value="todos">
  <label for="opctodos">Mostrar todos.</label><br><br>
  <input type="text" name="matricula" id="matricula" placeholder="Matricula...">
  <br><br>
  <input type="submit" value="Enviar">
</form>
</body>
</html>